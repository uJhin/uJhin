
<div align="center">
  <h2>
      <a href="https://codeberg.org/uJhin">Yu Jhin</a>
  </h2>
  <br/>
  <div align="left">
    <img alt="uJhin's BTC" src="https://img.shields.io/badge/BTC-3NVw2seiTQddGQwc1apqudKxuTqebpyL3s-blue?style=flat-square&logo=bitcoin">
    <br/>
    <img alt="uJhin's ETH" src="https://img.shields.io/badge/ETH-0x60dd373f59862d9df776596889b997e24bee42eb-blue?style=flat-square&logo=ethereum">
  </div>
  <br/>
  <a href="https://github.com/uJhin">
    <img alt="uJhin's Crypto" src="https://github.com/uJhin/ujhin/raw/main/images/YuJhin_Crypto.png"/>
  </a>
  <br/>
</div>
